const express = require('express');
const path = require('path');
const multer = require('multer');
const indexRouter = require('./routes/index');
const uploadRouter = require('./routes/upload');
const bodyParser = require('body-parser');
const busboy = require('connect-busboy');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());
app.use(busboy({ immediate: true }))
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/upload', uploadRouter);

const server = app.listen(7480);
const io = require('./socket').init(server);

io.on('connection', socket => {
    console.log('Client connected!');
});


