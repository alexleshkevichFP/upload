$(function () {
    var socket = io(location.origin);

    let process = 0;
    let file = null;

    socket.on('chunk loaded', function (savedSize) {
        if (file) {
            let percent = (savedSize / file.size) * 100;

            if (Math.floor(percent) !== Math.floor(process)) {
                console.log(process)
                process = Math.floor(percent);

                $('.progress-bar').text(process + '%');
                $('.progress-bar').width(process + '%');
                console.log('SOCKET: ', process);
            }

        }

    });

    $('#upload-input').click(function () {
        $('.progress-bar').text('0%');
        $('.progress-bar').width('0%');
        file = null;
        process = 0;
    });

    $('#uploadBtn').on('click', function () {
        file = $('#upload-input').get(0).files[0];
        const comment = $('#comment-input').val();

        console.log(file);

        if (!file) {
            return;
        }

        const formData = new FormData();

        // formData.append('file', file, file.name);
        formData.append('comment', comment);
        formData.append('file', file, file.name);

        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                process = 0;
                const fileList = $('#uploadList');
                fileList.html('');
                data.forEach((item) => {
                    const template = `<li><a target="_blank" href="/upload/${item.name}">${item.name}</a> File comment: ${item.comment} </li>`;
                    fileList.append(template);
                })
            }
        });
    });

})