var express = require('express');
var fs = require('fs');
var path= require('path');
var router = express.Router();

const p = path.join(__dirname, '../data', 'upload-list.json')

const readFromFile = (cb) => {
  fs.readFile(p, (err, fileContent) => {
    if (err) {
      cb([]);
    } else {
      cb(JSON.parse(fileContent));
    }

  })
}

/* GET home page. */
router.get('/', function(req, res, next) {
  readFromFile((fileList) => {
    res.render('index', { title: 'Express upload', fileList });
  })
});

module.exports = router;
