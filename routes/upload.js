const express = require('express');
const fs = require('fs');
const path = require('path');
const router = express.Router();
const io = require('../socket');


const p = path.join(__dirname, '../data', 'upload-list.json');

const readFromFile = (cb) => {
    fs.readFile(p, (err, fileContent) => {
        if (err) {
            cb([]);
        } else {
            cb(JSON.parse(fileContent));
        }
    });
}

router.post('/', (req, res, next) => {
    let fileName = null;
    let fileComment = null;

    req.busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
        fileName = filename;

        const p = path.join(__dirname, '../', 'uploads', 'storage', filename);
        const ws = fs.createWriteStream(p);
        let dataLength = 0;

        file.on('data', function(chunk) {
            // console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
            dataLength += chunk.length;
            console.log(dataLength)
            ws.write(chunk);
            io.getIO().emit('chunk loaded', dataLength);
        });
    });

    req.busboy.on('field', (fieldname, val) => {
        fileComment = val;
    });

    req.busboy.on('finish', () => {
        console.log(fileName, fileComment);
    });

        readFromFile((filesList) => {
            filesList.push({name: fileName, comment: fileComment});

            fs.writeFile(p, JSON.stringify(filesList), (err) => {
                if (err) {
                    console.log('ERR: ', err);
                } else {
                    console.log(filesList)
                    res.json(filesList);
                }
            });
        });

});

router.get('/:name', (req, res) => {

    const fileName = req.params.name;
    const downloadDirPath = path.join(__dirname, '../', 'uploads', 'storage')
    res.download(downloadDirPath + '/' + fileName, fileName, (err) => {
        if (err) {
            res.status(500).send({
                message: "Could not download the file. " + err
            });
        }

    });
})

module.exports = router;